package hello.kimun.com.kimunfinal02;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import hello.kimun.com.kimunfinal02.IntroApp.SecondLayoutIntro;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, SecondLayoutIntro.class);
        startActivity(intent);
        setContentView(R.layout.intro_2);


    }


    public void cerrarTodo(View v)
    {

        finish();


    }


}
