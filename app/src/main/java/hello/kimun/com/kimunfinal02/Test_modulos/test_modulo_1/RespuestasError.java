package hello.kimun.com.kimunfinal02.Test_modulos.test_modulo_1;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

import hello.kimun.com.kimunfinal02.R;

/**
 * Created by Usuario_perfil on 02-07-2015.
 */
public class RespuestasError extends Activity {
    public String usuario;
    TextView usuario1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Codigo para full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //aqui termina Codigo para full screen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preguntas_error);
        Bundle extras = getIntent().getExtras();
        usuario = extras.getString("Usuario_perfil");
        Log.d("que es esto!", usuario.toString());
        usuario1 = (TextView) findViewById(R.id.usuario);
        usuario1.setText(usuario);


    }
}