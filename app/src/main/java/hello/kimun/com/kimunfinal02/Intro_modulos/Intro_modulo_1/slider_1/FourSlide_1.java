package hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.slider_1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.intro1;
import hello.kimun.com.kimunfinal02.R;

/**
 * Created by Usuario_perfil on 24-06-2015.
 */
public class FourSlide_1 extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.modulo_1_intro4, container, false);
        return v;

    }
}