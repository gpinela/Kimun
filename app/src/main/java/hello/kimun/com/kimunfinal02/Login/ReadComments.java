package hello.kimun.com.kimunfinal02.Login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import hello.kimun.com.kimunfinal02.R;
import hello.kimun.com.kimunfinal02.Test_modulos.test_modulo_1.test_modulo_1.Intro_test_modulo_1;
import hello.kimun.com.kimunfinal02.Usuario.Usuario_perfil;
import hello.kimun.com.kimunfinal02.estudiar_modulos.MainActivitys;



/**
 * Created by Usuario_perfil on 15-06-2015.
 */
public class ReadComments extends Activity {
public String usuario;
 Button boton;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        //Codigo para full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //aqui termina Codigo para full screen
       super.onCreate(savedInstanceState);
       setContentView(R.layout.read_comments);
        Bundle extras = getIntent().getExtras();
        usuario=extras.getString("Usuario_perfil");
        Log.d("que es esto!", usuario.toString());
        boton = (Button) findViewById(R.id.button);
   }

    public void onClick1(View v) {
        Intent intent = new Intent(this, MainActivitys.class);
        intent.putExtra("Usuario_perfil", usuario);
        Log.d("que es esto!", usuario.toString());
        startActivity(intent);

    }
    public void onClickTest(View v) {
        Intent intent = new Intent(this, Intro_test_modulo_1.class);
        intent.putExtra("Usuario_perfil", usuario);
        Log.d("que es esto!", usuario.toString());
        startActivity(intent);

    }
    public void onClickUser(View v) {
        Intent intent = new Intent(this, Usuario_perfil.class);
        intent.putExtra("Usuario_perfil", usuario);
        Log.d("que es esto!", usuario.toString());
        startActivity(intent);

    }
    public void cerrar(View v)
    {

        moveTaskToBack(true);


    }
}
