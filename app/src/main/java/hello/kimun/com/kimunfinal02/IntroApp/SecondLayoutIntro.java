package hello.kimun.com.kimunfinal02.IntroApp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro2;

import hello.kimun.com.kimunfinal02.Login.Login;
import hello.kimun.com.kimunfinal02.IntroApp.slides2.FirstSlide2;
import hello.kimun.com.kimunfinal02.IntroApp.slides2.SecondSlide2;
import hello.kimun.com.kimunfinal02.IntroApp.slides2.ThirdSlide2;

/**
 * Created by Usuario_perfil on 15-06-2015.
 */
public class SecondLayoutIntro extends AppIntro2 {
    @Override
    public void init(Bundle savedInstanceState) {
        addSlide(new FirstSlide2(), getApplicationContext());
        addSlide(new SecondSlide2(), getApplicationContext());
        addSlide(new ThirdSlide2(), getApplicationContext());
    }

    private void loadMainActivity(){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    @Override
    public void onDonePressed() {
        loadMainActivity();
    }

    public void getStarted(View v){
        loadMainActivity();
    }
}
