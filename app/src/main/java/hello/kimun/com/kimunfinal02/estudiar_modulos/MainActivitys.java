package hello.kimun.com.kimunfinal02.estudiar_modulos;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.intro1;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_10.intro10;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_2.intro2;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_3.intro3;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_4.intro4;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_5.intro5;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_6.intro6;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_7.intro7;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_8.intro8;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_9.intro9;
import hello.kimun.com.kimunfinal02.R;

/**
 * Created by Usuario_perfil on 17-06-2015.
 */
public class MainActivitys extends Activity {

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser2 jParser = new JSONParser2();

    ArrayList<HashMap<String, String>> empresaList;


    // url to get all products list
    private static String url_all_empresas = "http://kimunapp.cl/BaseDatos/get_all_empresas.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "empresas";
    private static final String TAG_ID = "id";
    private static final String TAG_NOMBRE = "modulo";
    private int puntero;
    public String usuario;
    // products JSONArray
    JSONArray products = null;

    ListView lista;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Codigo para full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //aqui termina Codigo para full screen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        Bundle extras = getIntent().getExtras();
        usuario=extras.getString("Usuario_perfil");
        Log.d("que es esto!", usuario.toString());
        // Hashmap para el ListView
        empresaList = new ArrayList<HashMap<String, String>>();

        // Cargar los productos en el Background Thread
        new LoadAllProducts().execute();
        lista = (ListView) findViewById(R.id.listAllProducts);

       // ActionBar actionBar = getSupportActionBar();
       // actionBar.setDisplayHomeAsUpEnabled(true);
        // acción cuando hacemos click en item para poder modificarlo o eliminarlo

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                long auxId = lista.getItemIdAtPosition(position);
                Log.e("ver", "onItemClick " +auxId);
                puntero= (int)auxId;
                switch (puntero) {
                    case 0:
                      //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(MainActivitys.this, intro1.class );
                        startActivity(i);
                    break;

                    case 1:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent a = new Intent(MainActivitys.this, intro2.class );
                        startActivity(a);
                        break;

                    case 2:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i2 = new Intent(MainActivitys.this, intro3.class );
                        startActivity(i2);
                        break;
                    case 3:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i3 = new Intent(MainActivitys.this, intro4.class );
                        startActivity(i3);
                        break;
                    case 4:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i4 = new Intent(MainActivitys.this, intro5.class );
                        startActivity(i4);
                        break;
                    case 5:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i5 = new Intent(MainActivitys.this, intro6.class );
                        startActivity(i5);
                        break;
                    case 6:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i6 = new Intent(MainActivitys.this, intro7.class );
                        startActivity(i6);
                        break;
                    case 7:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i7 = new Intent(MainActivitys.this, intro8.class );
                        startActivity(i7);
                        break;
                    case 8:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i8 = new Intent(MainActivitys.this, intro9.class );
                        startActivity(i8);
                        break;
                    case 9:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i9 = new Intent(MainActivitys.this, intro10.class );
                        startActivity(i9);
                        break;
                }
            }
        });





    }//fin onCreate


    class LoadAllProducts extends AsyncTask<String, String, String> {

        /**
         * Antes de empezar el background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivitys.this);
            pDialog.setMessage("Cargando Modulos. Por favor espere...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obteniendo todos los productos
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List params = new ArrayList();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_empresas, "GET", params);

            // Check your log cat for JSON reponse
            Log.d("All Modulos: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    products = json.getJSONArray(TAG_PRODUCTS);

                    // looping through All Products
                    //Log.i("ramiro", "produtos.length" + products.length());
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NOMBRE);

                        // creating new HashMap
                        HashMap map = new HashMap();

                        // adding each child node to HashMap key => value
                        map.put(TAG_ID, id);
                        map.put(TAG_NOMBRE, name);

                        empresaList.add(map);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            MainActivitys.this,
                            empresaList,
                            R.layout.single_post1,
                            new String[] {
                                  // TAG_ID,
                                    TAG_NOMBRE,
                            },
                            new int[] {
                                    //R.id.single_post_tv_id,
                                    R.id.single_post_tv_nombre,
                            }



                    );
                   // updating listview
                   // setListAdapter(adapter);
                    lista.setAdapter(adapter);

                }
            });
        }


    }



}
