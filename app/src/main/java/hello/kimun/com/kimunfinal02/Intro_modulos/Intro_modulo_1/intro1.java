package hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.FrameStats;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import com.github.paolorotolo.appintro.AppIntro2;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.slider_1.FirstSlide_1;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.slider_1.FiveSlide_1;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.slider_1.FourSlide_1;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.slider_1.SecondSlide_1;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.slider_1.SixSlide_1;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.slider_1.ThirdSlide_1;
import hello.kimun.com.kimunfinal02.R;

/**
 * Created by Usuario_perfil on 21-06-2015.
 */
public class intro1  extends AppIntro2 {

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParserIntro1 jParser = new JSONParserIntro1();

    ArrayList<HashMap<String, String>> IntroList;


    // url to get all products list
    private static String url_all_Intro = "http://kimunapp.cl/modulos/get_all_intro_modulo1.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "intro_modulo";
    private static final String TAG_ID = "orden";
    private static final String TAG_DETALLE = "detalle";
    public String text_saludo_intro1_0;
    public String text_saludo_intro1_1;
    public String text_saludo_intro1_2;
    public String text_saludo_intro1_3;
    public String text_saludo_intro1_4;
    public String text_saludo_intro1_5;
    public String text_saludo_intro1_6;
    public String text_saludo_intro1_7;
    public String text_saludo_intro1_8;
    public String text_saludo_intro1_9;

    // products JSONArray
    JSONArray intro = null;

    ListView listaIntro;

    @Override
    public void init(Bundle savedInstanceState) {

        addSlide(new FirstSlide_1(), getApplicationContext());
        addSlide(new SecondSlide_1(), getApplicationContext());
        addSlide(new ThirdSlide_1(), getApplicationContext());
        addSlide(new FourSlide_1(), getApplicationContext());
        addSlide(new FiveSlide_1(), getApplicationContext());
        //addSlide(new SixSlide_1(), getApplicationContext());



        // Hashmap para el ListView
        IntroList = new ArrayList<HashMap<String, String>>();

        // Cargar los productos en el Background Thread
        new LoadAllmodulos().execute();

    }

    private void loadMainActivity(){
       Intent intent = new Intent(this, intro1_1.class);

        startActivity(intent);
    }

    @Override
    public void onDonePressed() {
        loadMainActivity();
    }

    public void getStarted(View v){
        loadMainActivity();
    }


    class LoadAllmodulos extends AsyncTask<String, String, String> {

        /**
         * Antes de empezar el background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(intro1.this);
            pDialog.setMessage("Cargando Intro. Por favor espere...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obteniendo todos los productos
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List params = new ArrayList();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_Intro, "GET", params);

            // Check your log cat for JSON reponse
            Log.d("All intro Modulo 1: ", json.toString());

            try {
            // Checking for SUCCESS TAG
            int success = json.getInt(TAG_SUCCESS);

            if (success == 1) {
                // products found
                // Getting Array of Products
                intro = json.getJSONArray(TAG_PRODUCTS);
                JSONObject a= intro.getJSONObject(0);
                text_saludo_intro1_0 = a.getString(TAG_DETALLE);
                Log.d("All intro Modulo 1: ", text_saludo_intro1_0);
                JSONObject b= intro.getJSONObject(1);
                text_saludo_intro1_1 = b.getString(TAG_DETALLE);
                Log.d("All intro Modulo 1: ", text_saludo_intro1_1);
                JSONObject c= intro.getJSONObject(2);
                text_saludo_intro1_2 = c.getString(TAG_DETALLE);
                Log.d("All intro Modulo 1: ", text_saludo_intro1_2);
                JSONObject d= intro.getJSONObject(3);
                text_saludo_intro1_3 = d.getString(TAG_DETALLE);
                Log.d("All intro Modulo 1: ", text_saludo_intro1_3);
                JSONObject e= intro.getJSONObject(4);
                text_saludo_intro1_4 = e.getString(TAG_DETALLE);
                Log.d("All intro Modulo 1: ", text_saludo_intro1_4);
                JSONObject f= intro.getJSONObject(5);
                text_saludo_intro1_5 = f.getString(TAG_DETALLE);
                Log.d("All intro Modulo 1: ", text_saludo_intro1_5);
                JSONObject g= intro.getJSONObject(6);
                text_saludo_intro1_6 = g.getString(TAG_DETALLE);
                Log.d("All intro Modulo 1: ", text_saludo_intro1_6);
                JSONObject h= intro.getJSONObject(7);
                text_saludo_intro1_7 = h.getString(TAG_DETALLE);
                Log.d("All intro Modulo 1: ", text_saludo_intro1_7);
                JSONObject i= intro.getJSONObject(8);
                text_saludo_intro1_8 = i.getString(TAG_DETALLE);
                Log.d("All intro Modulo 1: ", text_saludo_intro1_8);
                JSONObject j= intro.getJSONObject(9);
                text_saludo_intro1_9 = j.getString(TAG_DETALLE);
                Log.d("All intro Modulo 1: ", text_saludo_intro1_9);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(String file_url) {
        pDialog.dismiss();





    }

    }



    }




