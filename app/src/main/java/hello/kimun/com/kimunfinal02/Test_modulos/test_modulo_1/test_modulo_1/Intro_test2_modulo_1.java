package hello.kimun.com.kimunfinal02.Test_modulos.test_modulo_1.test_modulo_1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import hello.kimun.com.kimunfinal02.R;
import hello.kimun.com.kimunfinal02.Test_modulos.test_modulo_1.RespuestasError;
import hello.kimun.com.kimunfinal02.Test_modulos.test_modulo_1.RespuetasCorrectas;

/**
 * Created by Usuario_perfil on 02-07-2015.
 */
public class Intro_test2_modulo_1 extends Activity {

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParserTest1 jParser = new JSONParserTest1();

    ArrayList<HashMap<String, String>> testList;


    // url to get all products list
    private static String url_test = "http://kimunapp.cl/test/test_modulo_1.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_TEST = "test_modulo_1";
    private static final String TAG_PREGUNTA = "pregunta";
    private static final String TAG_CORRECTA = "respuesta_correcta";
    private static final String TAG_ERROR1 = "error_1";
    private static final String TAG_ERROR2 = "error_2";
    public int puntero;
    public int puntero2;
    public String usuario;
    public String text_pregunta_modulo_1_0;
    public String text_pregunta_modulo_1_correcta;
    public String text_pregunta_modulo_1_error1;
    public String text_pregunta_modulo_1_error2;
    // products JSONArray
    JSONArray tests = null;

    ListView lista;
    TextView pregunta;
    Button correcto, error1, error2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Codigo para full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //aqui termina Codigo para full screen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_modulo_1);
        Bundle extras = getIntent().getExtras();
        usuario = extras.getString("Usuario_perfil");
        puntero= extras.getInt("puntero");
        Log.d("que es esto!", usuario.toString());
        // Hashmap para el ListView
        testList = new ArrayList<HashMap<String, String>>();

        // Cargar los productos en el Background Thread
        new LoadAlltest().execute();


    }//fin onCreate


    class LoadAlltest extends AsyncTask<String, String, String> {

        /**
         * Antes de empezar el background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Intro_test2_modulo_1.this);
            pDialog.setMessage("Cargando Intro. Por favor espere...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obteniendo todos los productos
         */
        protected String doInBackground(String... args) {
            // Building Parameters
            List params = new ArrayList();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_test, "GET", params);

            // Check your log cat for JSON reponse
            Log.d("All test 1: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    tests = json.getJSONArray(TAG_TEST);
                    puntero2=tests.length();

                    if(puntero!=puntero2) {
                        puntero2=puntero2-puntero;
                        JSONObject a = tests.getJSONObject(puntero2);
                        text_pregunta_modulo_1_0 = a.getString(TAG_PREGUNTA);
                        Log.d("All intro Modulo 1: ", text_pregunta_modulo_1_0);
                        text_pregunta_modulo_1_correcta = a.getString(TAG_CORRECTA);
                        text_pregunta_modulo_1_error1 = a.getString(TAG_ERROR1);
                        text_pregunta_modulo_1_error2 = a.getString(TAG_ERROR2);
                    }
                    else{
                        pDialog.dismiss();
                        finish();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();


            pregunta = (TextView) findViewById(R.id.id_pregunta);
            pregunta.setText(text_pregunta_modulo_1_0);

            correcto = (Button) findViewById(R.id.single1);
            correcto.setText(text_pregunta_modulo_1_correcta);

            error1 = (Button) findViewById(R.id.single);
            error1.setText(text_pregunta_modulo_1_error1);

            error2 = (Button) findViewById(R.id.single2);
            error2.setText(text_pregunta_modulo_1_error2);

        }

    }
    public void OnclickCorrecto(View view){

        Intent intent = new Intent(this, RespuetasCorrectas.class);
        intent.putExtra("Usuario_perfil", usuario);
        intent.putExtra("pregunta",text_pregunta_modulo_1_0);
        intent.putExtra("correcto",text_pregunta_modulo_1_correcta);
        Log.d("que es esto!", usuario.toString());
        startActivity(intent);

    }

    public void OnclickError(View view){

        Intent intent = new Intent(this, RespuestasError.class);
        intent.putExtra("Usuario_perfil", usuario);
        intent.putExtra("pregunta",text_pregunta_modulo_1_0);
        intent.putExtra("correcto",text_pregunta_modulo_1_correcta);
        Log.d("que es esto!", usuario.toString());
        startActivity(intent);

    }
    public void OnclickSiguiente(View view){

        Intent intent = new Intent(this, Intro_test2_modulo_1.class);
        intent.putExtra("Usuario_perfil", usuario);
        intent.putExtra("pregunta",text_pregunta_modulo_1_0);
        intent.putExtra("correcto",text_pregunta_modulo_1_correcta);
        puntero=puntero+1;
        Log.isLoggable("que es esto!", puntero);
        intent.putExtra("puntero",puntero);
        Log.d("que es esto!", usuario.toString());
        startActivity(intent);

    }
    public void OnclickAtras(View view){

        Intent intent = new Intent(this, Intro_test_modulo_1.class);
        intent.putExtra("Usuario_perfil", usuario);
        intent.putExtra("pregunta",text_pregunta_modulo_1_0);
        intent.putExtra("correcto",text_pregunta_modulo_1_correcta);
        puntero=puntero-1;
        Log.isLoggable("que es esto!", puntero);
        intent.putExtra("puntero",puntero);
        Log.d("que es esto!", usuario.toString());
        startActivity(intent);

    }
}

