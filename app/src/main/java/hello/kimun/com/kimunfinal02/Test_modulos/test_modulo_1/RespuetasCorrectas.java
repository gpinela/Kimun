package hello.kimun.com.kimunfinal02.Test_modulos.test_modulo_1;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import hello.kimun.com.kimunfinal02.R;

/**
 * Created by Usuario_perfil on 02-07-2015.
 */
public class RespuetasCorrectas extends Activity {
    public String usuario;
    public String pregunta;
    public String respuesta_correcta;
   TextView usuario1, pregunta1, respuesta_correcta1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Codigo para full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //aqui termina Codigo para full screen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.correcto);
        Bundle extras = getIntent().getExtras();
        usuario = extras.getString("Usuario_perfil");
        pregunta = extras.getString("pregunta");
        respuesta_correcta = extras.getString("correcto");
        Log.d("que es esto!", usuario.toString());
        Log.d("que es esto!", pregunta.toString());
        Log.d("que es esto!", respuesta_correcta.toString());
        usuario1=(TextView)findViewById(R.id.usuario);
        usuario1.setText(usuario);
        pregunta1=(TextView)findViewById(R.id.pregunta);
        pregunta1.setText(pregunta);
        respuesta_correcta1=(TextView)findViewById(R.id.respuesta1);
        respuesta_correcta1.setText(respuesta_correcta);


    }
}