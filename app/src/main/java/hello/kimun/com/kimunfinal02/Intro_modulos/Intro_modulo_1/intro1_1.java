package hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import hello.kimun.com.kimunfinal02.R;
import hello.kimun.com.kimunfinal02.estudiar_modulos.MainActivitys;

/**
 * Created by Usuario_perfil on 21-06-2015.
 */
public class intro1_1 extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState){
        //Codigo para full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //aqui termina Codigo para full screen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modulo_1_menu);

    }

    public void volverOnClick(View v)
    {
        Intent intent = new Intent(this, intro1.class);
        startActivity(intent);
    }
    public void repasoOnClick(View v)
    {
        Intent intent1 = new Intent(this, intro_palabras.class);
        startActivity(intent1);
    }

    public void SalirOnClick(View v)
    {
       finish();
    }



}
