package hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro2;

import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_2.slider_2.FirstSlide_2;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_2.slider_2.SecondSlide_2;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_2.slider_2.ThirdSlide_2;

/**
 * Created by Usuario_perfil on 24-06-2015.
 */
public class intro2 extends AppIntro2 {


    @Override
    public void init(Bundle savedInstanceState) {

        addSlide(new FirstSlide_2(), getApplicationContext());
        addSlide(new SecondSlide_2(), getApplicationContext());
        addSlide(new ThirdSlide_2(), getApplicationContext());

    }

    private void loadMainActivity2(){
        Intent intent = new Intent(this, intro2_2.class);
        startActivity(intent);
    }

    @Override
    public void onDonePressed() {
        loadMainActivity2();
    }

    public void getStarted(View v){
        loadMainActivity2();
    }




}