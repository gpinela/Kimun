package hello.kimun.com.kimunfinal02.Usuario;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaCodec.BufferInfo.*;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import hello.kimun.com.kimunfinal02.Login.ReadComments;
import hello.kimun.com.kimunfinal02.R;

/**
 * Created by Usuario_perfil on 02-07-2015.
 */
public class Usuario_perfil extends Activity {
    public String usuario;
    Button boton;
    private Button bt;

    //Creamos el handler puente para mostrar
//el mensaje recibido del servidor
    private Handler puente = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //Mostramos el mensage recibido del servido en pantalla
            Toast.makeText(getApplicationContext(), (String) msg.obj,
                    Toast.LENGTH_LONG).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Codigo para full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //aqui termina Codigo para full screen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usuario);
        Bundle extras = getIntent().getExtras();
        usuario = extras.getString("Usuario_perfil");
        TextView user = (TextView) findViewById(R.id.usuario_user);
        user.setText(usuario);
        Log.d("que es esto!", usuario.toString());
        boton = (Button) findViewById(R.id.button);
        bt = (Button)findViewById(R.id.enviar_datos_usuario);

        //Añadimos el Listener
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Hay que hacerlo dentro del Thread
                //No me dejaba tocar la Clase de Network
                //directamente en el hilo principal
                Thread thr = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //Enviamos el texto escrito a la funcion
                        EnviarDatos(usuario.toString());
                        EnviarDatos("usuario");
                        EnviarDatos(usuario.toString());
                    }
                });
                //Arrancamos el Hilo
                thr.start();
            }
        });



    }

    public void OnclickAtrasUser(View view){
        Intent intent = new Intent(this, ReadComments.class);
        intent.putExtra("Usuario_perfil", usuario);
        Log.d("que es esto!", usuario.toString());
        startActivity(intent);

    }

    private void EnviarDatos(String dato){
        //Utilizamos la clase Httpclient para conectar
        HttpClient httpclient = new DefaultHttpClient();
        //Utilizamos la HttpPost para enviar lso datos
        //A la url donde se encuentre nuestro archivo receptor
        HttpPost httppost = new HttpPost("http://kimunapp.cl/usuarios/index.php");
        try {
            //Añadimos los datos a enviar en este caso solo uno
            //que le llamamos de nombre 'a'
            //La segunda linea podría repetirse tantas veces como queramos
            //siempre cambiando el nombre ('a')
            List<NameValuePair> postValues = new ArrayList<NameValuePair>(2);
            postValues.add(new BasicNameValuePair("a", dato));
            postValues.add(new BasicNameValuePair("b", dato));
            postValues.add(new BasicNameValuePair("c", dato));
            //Encapsulamos
            httppost.setEntity(new UrlEncodedFormEntity(postValues));
            //Lanzamos la petición
            HttpResponse respuesta = httpclient.execute(httppost);
            //Conectamos para recibir datos de respuesta
            HttpEntity entity = respuesta.getEntity();
            //Creamos el InputStream como su propio nombre indica
            InputStream is = entity.getContent();
            //Limpiamos el codigo obtenido atraves de la funcion
            //StreamToString explicada más abajo
            String resultado= StreamToString(is);

            //Enviamos el resultado LIMPIO al Handler para mostrarlo
            Message sms = new Message();
            sms.obj = resultado;
            puente.sendMessage(sms);
        }catch (IOException e) {
            //TODO Auto-generated catch block
        }
    }

    //Funcion para 'limpiar' el codigo recibido
    public String StreamToString(InputStream is) {
        //Creamos el Buffer
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            //Bucle para leer todas las líneas
            //En este ejemplo al ser solo 1 la respuesta
            //Pues no haría falta
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //retornamos el codigo límpio
        return sb.toString();
    }


    public void ejecutar_sonido(View v) {
        MediaPlayer mp = new MediaPlayer();
        try {
            mp.setDataSource("http://fiestoforo.cl/dungun/chumlen/ogg/01_kvmelkalen.ogg");
            mp.prepare();
            mp.start();
        } catch (IOException e) {
        }
    }







}


