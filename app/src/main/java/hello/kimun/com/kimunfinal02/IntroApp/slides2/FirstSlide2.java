package hello.kimun.com.kimunfinal02.IntroApp.slides2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hello.kimun.com.kimunfinal02.R;

/**
 * Created by Usuario_perfil on 15-06-2015.
 */
public class FirstSlide2 extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.intro_2, container, false);
        return v;
    }
}
