package hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import hello.kimun.com.kimunfinal02.R;
import hello.kimun.com.kimunfinal02.estudiar_modulos.JSONParser2;

/**
 * Created by Usuario_perfil on 24-06-2015.
 */
public class intro_palabras extends Activity {

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser2 jParser = new JSONParser2();

    ArrayList<HashMap<String, String>> empresaList;


    // url to get all products list
    private static String url_all_empresas = "http://kimunapp.cl/BaseDatos/get_all_empresas.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "empresas";
    private static final String TAG_ID = "id";
    private static final String TAG_NOMBRE = "modulo";
    private int puntero;

    // products JSONArray
    JSONArray products = null;

    ListView lista;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Codigo para full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //aqui termina Codigo para full screen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modulo_1_index);
        //llamada a imagen desde URL de internet
      //  ImageView imageView1= (ImageView) findViewById(R.id.imagen);
      //  Picasso.with(this).load("https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Wenufoye_redonda.svg/600px-Wenufoye_redonda.svg.png").into(imageView1);
        // Hashmap para el ListView
        empresaList = new ArrayList<HashMap<String, String>>();

        // Cargar los productos en el Background Thread
        new LoadAllProducts().execute();
        lista = (ListView) findViewById(R.id.listAllProducts1);

        // ActionBar actionBar = getSupportActionBar();
        // actionBar.setDisplayHomeAsUpEnabled(true);
        // acción cuando hacemos click en item para poder modificarlo o eliminarlo

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                long auxId = lista.getItemIdAtPosition(position);
                Log.e("ver", "onItemClick " + auxId);
                puntero= (int)auxId;
                switch (puntero) {
                    case 0:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(intro_palabras.this, intro1_1.class );
                        startActivity(i);
                        break;

                    case 1:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent a = new Intent(intro_palabras.this, intro1_1.class );
                        startActivity(a);
                        break;

                    case 2:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i2 = new Intent(intro_palabras.this, intro1_1.class );
                        startActivity(i2);
                        break;
                    case 3:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i3 = new Intent(intro_palabras.this, intro1_1.class );
                        startActivity(i3);
                        break;
                    case 4:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i4 = new Intent(intro_palabras.this, intro1_1.class );
                        startActivity(i4);
                        break;
                    case 5:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i5 = new Intent(intro_palabras.this, intro1_1.class );
                        startActivity(i5);
                        break;
                    case 6:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i6 = new Intent(intro_palabras.this, intro1_1.class );
                        startActivity(i6);
                        break;
                    case 7:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i7 = new Intent(intro_palabras.this, intro1_1.class );
                        startActivity(i7);
                        break;
                    case 8:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i8 = new Intent(intro_palabras.this, intro1_1.class );
                        startActivity(i8);
                        break;
                    case 9:
                        //  Toast.makeText(getApplicationContext(), "Haz hecho click en " + puntero, Toast.LENGTH_SHORT).show();
                        Intent i9 = new Intent(intro_palabras.this, intro1_1.class );
                        startActivity(i9);
                        break;
                }
            }
        });





    }//fin onCreate


    class LoadAllProducts extends AsyncTask<String, String, String> {

        /**
         * Antes de empezar el background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(intro_palabras.this);
            pDialog.setMessage("Cargando Modulos. Por favor espere...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obteniendo todos los productos
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List params = new ArrayList();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_empresas, "GET", params);

            // Check your log cat for JSON reponse
            Log.d("All Modulos: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    products = json.getJSONArray(TAG_PRODUCTS);

                    // looping through All Products
                    //Log.i("ramiro", "produtos.length" + products.length());
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NOMBRE);

                        // creating new HashMap
                        HashMap map = new HashMap();

                        // adding each child node to HashMap key => value
                        map.put(TAG_ID, id);
                        map.put(TAG_NOMBRE, name);

                        empresaList.add(map);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            intro_palabras.this,
                            empresaList,
                            R.layout.single_post1_modulo1_index,
                            new String[] {
                                    // TAG_ID,
                                    TAG_NOMBRE,
                            },
                            new int[] {
                                    //R.id.single_post_tv_id,
                                    R.id.single_post_tv_nombre1,
                            }

                    );
                    // updating listview
                    // setListAdapter(adapter);
                    lista.setAdapter(adapter);

                }
            });
        }


    }


}
