package hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.slider_1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.intro1;
import hello.kimun.com.kimunfinal02.R;

/**
 * Created by Usuario_perfil on 21-06-2015.
 */
public class SecondSlide_1 extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.modulo_1_intro2, container, false);

        return v;
    }
}