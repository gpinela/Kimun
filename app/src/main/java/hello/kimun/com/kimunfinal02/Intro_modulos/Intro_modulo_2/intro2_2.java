package hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.intro1;
import hello.kimun.com.kimunfinal02.Intro_modulos.Intro_modulo_1.intro_palabras;
import hello.kimun.com.kimunfinal02.R;

/**
 * Created by Usuario_perfil on 24-06-2015.
 */
public class intro2_2 extends Activity{


    @Override
    protected void onCreate(Bundle savedInstanceState){
        //Codigo para full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //aqui termina Codigo para full screen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modulo_2_menu);

    }

    public void volverOnClick2(View v)
    {
        Intent intent = new Intent(this, intro2.class);
        startActivity(intent);
    }
    public void repasoOnClick2(View v)
    {
        Intent intent = new Intent(this, intro_palabras2.class);
        startActivity(intent);
    }

    public void SalirOnClick2(View v)
    {
        finish();
    }


}
